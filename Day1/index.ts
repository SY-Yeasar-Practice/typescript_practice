const express = require('express');
const app = express ()
const mongoose = require('mongoose')
const cors = require('cors')
require("dotenv").config ()
//global middle ware part
app.use (cors ());

//dot env file 
const PORT = process.env.PORT || 8080
const url = process.env.mongoURL

//connect to the database
mongoose.connect (url, {
    useNewUrlParser: true
   
})
.then (() => {console.log (`Server is connected to the database`)})
.catch ((err:any) => console.log(err))
app.listen (PORT, () => {
    console.log(`Server is running on ${PORT}`)
})

