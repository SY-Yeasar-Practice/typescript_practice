interface ResultOfUser {
    name: string
    age: number
    getResult (): void
}

export default ResultOfUser