import ResultOfUser from "../Interfaces/interfaces"

class User {
    name: string
    age: number
    constructor (name:string, age: number) {
        this.name = name,
        this.age = age
    }
    intro (): void {}
}

class Student extends User implements ResultOfUser {
    constructor (name:string, age:number) {
        super(name, age)
    }
    intro ():void {
        console.log(`Mey name is ${this.name} I am ${this.age} years old`)
    }
    getResult (): void {
        console.log("I got A +")
    }
}

export {
    Student,
    User,
    ResultOfUser
}