type obj = {
    name: string,
    age: number
}
type hello = (person: obj) => number

export {
    obj,
    hello
}